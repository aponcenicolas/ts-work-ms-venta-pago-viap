package lux.pe.na;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VentaPagoViapApplication {

	public static void main(String[] args) {
		SpringApplication.run(VentaPagoViapApplication.class, args);
	}

}
