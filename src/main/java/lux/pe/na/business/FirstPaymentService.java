package lux.pe.na.business;

import lux.pe.na.expose.request.PrimerPagoRequest;
import lux.pe.na.expose.response.Response;
import reactor.core.publisher.Mono;

import java.util.List;


public interface FirstPaymentService {

  Mono<Response> getAllFirstPayment(Integer multipleRequestId);

  Mono<Response> updateFirstPayment(Integer multipleRequestId, List<PrimerPagoRequest> firstPaymentRequestList);

}
