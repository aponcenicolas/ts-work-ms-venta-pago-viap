package lux.pe.na.business;

import lux.pe.na.expose.request.PagoRecurrenteRequest;
import lux.pe.na.expose.response.Response;
import reactor.core.publisher.Mono;

import java.util.List;

public interface RecurringPaymentService {

  Mono<Response> getAllRecurringPayment(Integer multipleRequestId);

  Mono<Response> updateRecurringPayment(Integer multipleRequestId,
                                    List<PagoRecurrenteRequest> recurringPaymentRequestList,
                                    Boolean useSamePaymentMethod);

}
