package lux.pe.na.business.impl;

import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lux.pe.na.business.FirstPaymentService;
import lux.pe.na.exception.PVException;
import lux.pe.na.expose.request.PrimerPagoRequest;
import lux.pe.na.expose.response.PrimerPagoResponse;
import lux.pe.na.expose.response.Response;
import lux.pe.na.mapper.PaymentMapper;
import lux.pe.na.model.dto.ItemValor;
import lux.pe.na.model.dto.SolicitudDto;
import lux.pe.na.model.entity.Solicitud;
import lux.pe.na.repository.RequestRepository;
import lux.pe.na.util.ConstantUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static lux.pe.na.util.ConstantUtil.*;

@Service
@AllArgsConstructor
@Slf4j
public class FirstPaymentServiceImpl implements FirstPaymentService {

  private final RequestRepository requestRepository;

  @Override
  public Mono<Response> getAllFirstPayment(Integer multipleRequestId) {
    List<SolicitudDto> requestDtoList = requestRepository.findAllRequest(multipleRequestId);

    List<PrimerPagoResponse> firstPaymentResponseList = requestDtoList.stream()
        .map(requestDto -> PrimerPagoResponse.builder()
            .idSolicitud(requestDto.getIdSolicitud())
            .cobranzaPrimeraCuota(ItemValor.builder()
                .codigo(requestDto.getCodigoTipoCobranzaPrimeraCuota())
                .descripcion(null)
                .build())
            .entidadPrimeraCuota(ItemValor.builder()
                .codigo(requestDto.getCodigoEntidadPrimeraCuota())
                .descripcion(null)
                .build())
            .fechaVencimientoSafetyPay(requestDto.getFechaVencimientoSafetyPay())
            .codigoSafetyPay(requestDto.getCodigoSafetyPay())
            .fechaPago(Strings.isNullOrEmpty(requestDto.getCodigoSafetyPay()) ? requestDto.getFechaOperacionTarjeta() : requestDto.getFechaPago())
            .numeroOperacionTarjeta(requestDto.getNumeroOperacionTarjeta())
            .esPagoLink(Boolean.TRUE.equals(requestDto.getFlagPagoLink()))
            .build()).collect(Collectors.toList());

    return Mono.just(Response.builder()
        .code(200)
        .status("success")
        .data(firstPaymentResponseList)
        .build());
  }

  @Override
  public Mono<Response> updateFirstPayment(Integer multipleRequestId, List<PrimerPagoRequest> firstPaymentRequestList) {

    List<Solicitud> requestList;
    String message = "";

    requestList = requestRepository.findByIdSolicitudMultiple(multipleRequestId);

    if (requestList.size() == 0) {
      log.info("No existen solicitudes para la Solicitud Multiple con el ID {}", multipleRequestId);
      throw new PVException("No existen solicitudes para la Solicitud Multiple con el ID ".concat(String.valueOf(multipleRequestId)));

    }

    if (requestList.stream()
        .flatMap(request -> firstPaymentRequestList.stream()
            .filter(firstPaymentList -> request.getIdSolicitud().equals(firstPaymentList.getIdSolicitud())))
        .map(PrimerPagoRequest::getIdSolicitud).findAny().isEmpty()) {

      log.info("No existen las solicitudes con los ID {}", String.join(",", (CharSequence) firstPaymentRequestList
          .stream()
          .map(PrimerPagoRequest::getIdSolicitud)
          .collect(Collectors.toList())));

      throw new PVException("No existen las solicitudes con los ID "
          .concat(String.join(",", (CharSequence) firstPaymentRequestList
              .stream()
              .map(PrimerPagoRequest::getIdSolicitud)
              .collect(Collectors.toList()))));

    }

    List<Solicitud> listRequest = firstPaymentRequestList.stream()
        .map(PaymentMapper::firstPaymentRequestToRequest)
        .collect(Collectors.toList());

    if (firstPaymentRequestList.size() > 0) {
      List<Solicitud> listSave = requestList.stream()
          .flatMap(solicitud -> listRequest.stream()
              .map(request -> {
                validIdSolicitud(solicitud, request);
                return solicitud;
              })).collect(Collectors.toList());

      message = validMessage(listSave);
    }
    return Mono.just(Response.builder()
        .code(200)
        .status("success")
        .data(message)
        .build());
  }

  private void validIdSolicitud(Solicitud solicitud, Solicitud request) {
    if (solicitud.getIdSolicitud().equals(request.getIdSolicitud())) {

      solicitud.setIdSolicitud(request.getIdSolicitud());
      solicitud.setCodigoTipoCobranzaPrimeraCuota(request.getCodigoTipoCobranzaPrimeraCuota());
      solicitud.setUsuarioCreacion(request.getUsuarioCreacion());

      validCodigoEntidadPrimeraCuota(solicitud, request);
      validCodigoSafetyPay(solicitud, request);
    }
  }

  private void validCodigoEntidadPrimeraCuota(Solicitud solicitud, Solicitud request) {
    if (request.getCodigoEntidadPrimeraCuota() == 0) {
      solicitud.setCodigoEntidadPrimeraCuota(null);
    } else {
      solicitud.setCodigoEntidadPrimeraCuota(request.getCodigoEntidadPrimeraCuota());
    }
  }

  private void validCodigoSafetyPay(Solicitud solicitud, Solicitud request) {
    if (!StringUtils.isEmpty(request.getCodigoSafetyPay()) || request.getCodigoSafetyPay() != null) {
      solicitud.setCodigoSafetyPay(request.getCodigoSafetyPay());
      solicitud.setFechaVencimientoSafetyPay(request.getFechaVencimientoSafetyPay());
      solicitud.setFechaPago(request.getFechaPago());
      solicitud.setNumeroOperacionTarjeta(null);
      solicitud.setCodigoRespuestaTarjeta(null);
      solicitud.setFlagPagoLink(null);
      solicitud.setExternalId(null);
    } else {
      solicitud.setCodigoSafetyPay(null);
      solicitud.setFechaVencimientoSafetyPay(null);
      solicitud.setFechaOperacionTarjeta(request.getFechaPago());
      solicitud.setCodigoRespuestaTarjeta(VALUE_ONE);

      validNumeroOperacionTarjeta(solicitud, request);
      validFlagPagoLink(solicitud, request);
      validExternalId(solicitud, request);
    }
  }

  private void validNumeroOperacionTarjeta(Solicitud solicitud, Solicitud request) {
    if (request.getNumeroOperacionTarjeta() == null || StringUtils.isEmpty(request.getNumeroOperacionTarjeta())) {
      solicitud.setNumeroOperacionTarjeta(null);
    } else {
      solicitud.setNumeroOperacionTarjeta(request.getNumeroOperacionTarjeta());
    }
  }

  private String validMessage(List<Solicitud> listSave) {
    String message;
    if (listSave.size() > 0) {
      requestRepository.saveAll(listSave);
      message = MESSAGE_UPDATED_SUCCESS;
    } else {
      message = MESSAGE_UPDATED_ERROR;
    }
    return message;
  }

  private void validFlagPagoLink(Solicitud solicitud, Solicitud request) {
    if (request.getExternalId() == null || StringUtils.isEmpty(request.getExternalId())) {
      solicitud.setFlagPagoLink(null);
    } else {
      solicitud.setFlagPagoLink(1);
    }
  }

  private void validExternalId(Solicitud solicitud, Solicitud request) {
    if (request.getExternalId() == null || StringUtils.isEmpty(request.getExternalId())) {
      solicitud.setExternalId(null);
    } else {
      solicitud.setExternalId(request.getExternalId());
    }
  }

}
