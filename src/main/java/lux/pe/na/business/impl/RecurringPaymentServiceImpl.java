package lux.pe.na.business.impl;

import com.google.common.base.Strings;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lux.pe.na.business.RecurringPaymentService;
import lux.pe.na.encrypt.EncryptHelper;
import lux.pe.na.exception.PVException;
import lux.pe.na.expose.request.PagoRecurrenteRequest;
import lux.pe.na.expose.response.PagoRecurrenteResponse;
import lux.pe.na.expose.response.Response;
import lux.pe.na.mapper.PaymentMapper;
import lux.pe.na.model.dto.PagoRecurrenteDto;
import lux.pe.na.model.dto.SolicitudDto;
import lux.pe.na.model.entity.Solicitud;
import lux.pe.na.model.entity.SolicitudMultiple;
import lux.pe.na.repository.MultipleRequestRepository;
import lux.pe.na.repository.RequestRepository;
import lux.pe.na.util.ConstantUtil;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static lux.pe.na.util.ConstantUtil.*;

@Service
@AllArgsConstructor
@Slf4j
public class RecurringPaymentServiceImpl implements RecurringPaymentService {

  private final RequestRepository requestRepository;
  private final EncryptHelper encryptHelper;
  private final MultipleRequestRepository multipleRequestRepository;

  @Override
  public Mono<Response> getAllRecurringPayment(Integer multipleRequestId) {
    List<SolicitudDto> requestDtoList = requestRepository.findAllRequest(multipleRequestId);
    return Mono.just(Response.builder()
        .code(200)
        .status("success")
        .data(PagoRecurrenteResponse.builder()
            .usarMismoMedioPago(requestDtoList.stream().map(SolicitudDto::getUsarMismoMedioPago).findFirst().isPresent())
            .pagoRecurrenteDtoList(requestDtoList.stream().map(solicitudDto -> PagoRecurrenteDto.builder()
                .idSolicitud(solicitudDto.getIdSolicitud())
                .codigoTipoCobranza(solicitudDto.getCodigoTipoCobranza())
                .codigoOperadorTarjeta(solicitudDto.getCodigoEntidad())
                .codigoTipoCuenta(solicitudDto.getCodigoTipoCuenta())
                .build()).collect(Collectors.toList()))
            .build())
        .build());
  }

  @Override
  public Mono<Response> updateRecurringPayment(Integer multipleRequestId, List<PagoRecurrenteRequest> recurringPaymentRequestList,
                                               Boolean useSamePaymentMethod) {

    List<Solicitud> requestList;
    List<Integer> requestIdList;
    String message = "";

    requestList = requestRepository.findByIdSolicitudMultiple(multipleRequestId);

    if (requestList.size() != recurringPaymentRequestList.size()) {
      log.info("La cantidad de pagos  recurrentes enviados no coinciden con los registrados para la solicitud multiple con el ID {}",
          multipleRequestId);
      throw new PVException("La cantidad de pagos  recurrentes enviados no coinciden con los registrados para la solicitud multiple con el ID "
          .concat(String.valueOf(multipleRequestId)));
    } else {
      requestIdList = recurringPaymentRequestList.stream()
          .map(PagoRecurrenteRequest::getIdSolicitud)
          .collect(Collectors.toList());

      List<Integer> comparison = requestIdList.stream()
          .filter(id -> requestList.stream()
              .anyMatch(solicitudDto -> !solicitudDto.getIdSolicitud().equals(id)))
          .collect(Collectors.toList());

      if (!comparison.isEmpty()) {
        log.info("Los números de las solicitudes enviados no coinciden ID {}",
            multipleRequestId);
        throw new PVException("Los números de las solicitudes enviados no coinciden ID "
            .concat(String.valueOf(multipleRequestId)));
      }
    }

    if (recurringPaymentRequestList.size() > 0) {
      for (PagoRecurrenteRequest item : recurringPaymentRequestList) {
        item.setNumeroTarjeta(Strings.isNullOrEmpty(item.getNumeroTarjeta()) ? "" : encryptHelper.decryptRsaCode(item.getNumeroTarjeta()));
        item.setFechaVencimiento(Strings.isNullOrEmpty(item.getFechaVencimiento()) ? "" : encryptHelper.decryptRsaCode(item.getFechaVencimiento()));
      }
      List<Solicitud> listRequest = recurringPaymentRequestList.stream()
          .map(PaymentMapper::recurringPaymentToRequest)
          .collect(Collectors.toList());

      List<Solicitud> listSave = requestList.stream()
          .flatMap(solicitud -> listRequest.stream()
              .map(request -> {
                validIdSolicitud(solicitud, request);
                return solicitud;
              })).collect(Collectors.toList());

      message = validMessage(listSave, multipleRequestId, useSamePaymentMethod);
    }

    return Mono.just(Response.builder()
        .code(200)
        .status("success")
        .data(message)
        .build());
  }

  private String validMessage(List<Solicitud> listSave, Integer multipleRequestId, Boolean useSamePaymentMethod) {
    SolicitudMultiple multipleRequest = multipleRequestRepository.findById(multipleRequestId)
        .orElseThrow(() -> new PVException("No existe la solicitud multiple con el ID ".concat(String.valueOf(multipleRequestId))));
    String message;
    if (listSave.size() > 0) {
      multipleRequest.setFlagMismoMedioPagoRecurrente(useSamePaymentMethod);
      requestRepository.saveAll(listSave);
      multipleRequestRepository.save(multipleRequest);
      message = MESSAGE_UPDATED_SUCCESS;
    } else {
      message = MESSAGE_UPDATED_ERROR;
    }
    return message;
  }

  private void validIdSolicitud(Solicitud solicitud, Solicitud request) {
    if (solicitud.getIdSolicitud().equals(request.getIdSolicitud())) {
      solicitud.setIdSolicitud(request.getIdSolicitud());
      solicitud.setCodigoTipoCobranza(request.getCodigoTipoCobranza());
      solicitud.setUsuarioCreacion(request.getUsuarioCreacion());

      validCodigoEntidad(solicitud, request);
      validCodigoTipoCuenta(solicitud, request);
      validNumeroCuentaTarjeta(solicitud, request);
      validVencimientoTarjeta(solicitud, request);

    }
  }

  private void validVencimientoTarjeta(Solicitud solicitud, Solicitud request) {
    if (Strings.isNullOrEmpty(request.getVencimientoTarjeta())) {
      solicitud.setVencimientoTarjeta(request.getVencimientoTarjeta());
    } else {
      solicitud.setVencimientoTarjeta(null);
    }
  }

  private void validNumeroCuentaTarjeta(Solicitud solicitud, Solicitud request) {
    if (Strings.isNullOrEmpty(request.getNumeroCuentaTarjeta())) {
      solicitud.setNumeroCuentaTarjeta(request.getNumeroCuentaTarjeta());
    } else {
      solicitud.setNumeroCuentaTarjeta(null);
    }
  }

  private void validCodigoTipoCuenta(Solicitud solicitud, Solicitud request) {
    if (request.getCodigoTipoCuenta() > 0) {
      solicitud.setCodigoTipoCuenta(request.getCodigoTipoCuenta());
    } else {
      solicitud.setCodigoTipoCuenta(null);
    }
  }

  private void validCodigoEntidad(Solicitud solicitud, Solicitud request) {
    if (request.getCodigoEntidad() > 0) {
      solicitud.setCodigoEntidad(request.getCodigoEntidad());
    } else {
      solicitud.setCodigoEntidad(null);
    }
  }

}
