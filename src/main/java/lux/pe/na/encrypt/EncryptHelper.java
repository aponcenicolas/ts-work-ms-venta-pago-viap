package lux.pe.na.encrypt;

import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;

@Component
@Slf4j
public class EncryptHelper {

  @Value("${url}")
  private String privateCertificate;

  private static final String ALGORITHM_RSA = "RSA";

  @PostConstruct
  private void init() {
    Security.addProvider(new BouncyCastleProvider());
    log.info("Private certificate: " + privateCertificate);
  }

  /**
   * Decode BASE64 encoded string to bytes array.
   *
   * @param text The string.
   * @return String.
   */
  public String decryptRsaCode(String text) {
    try {
      PrivateKey privateKey = getPrivateKeyFromString(privateCertificate);
      return decrypt(text, privateKey);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      return null;
    }
  }

  /**
   * Decrypt text using private key.
   *
   * @param text The encrypted text.
   * @param key  The private key.
   * @return The unencrypted text.
   */
  private byte[] decrypt(byte[] text, PrivateKey key) throws NoSuchPaddingException, NoSuchAlgorithmException {
    byte[] decryptText = null;
    Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
    try {
      cipher.init(Cipher.DECRYPT_MODE, key);
    } catch (InvalidKeyException e) {
      log.info(e.getMessage());
    }
    try {
      decryptText = cipher.doFinal(text);
    } catch (IllegalBlockSizeException | BadPaddingException e) {
      log.info(e.getMessage());
    }
    return decryptText;

  }

  /**
   * Decrypt BASE64 encoded text using private key.
   *
   * @param text The encrypted text, encoded as BASE64.
   * @param key  The private key.
   * @return The unencrypted text encoded as UTF8.
   * @throws NoSuchAlgorithmException class.
   * @throws NoSuchPaddingException class.
   */
  private String decrypt(String text, PrivateKey key) throws NoSuchPaddingException, NoSuchAlgorithmException {
    String result;
    byte[] decryptText = decrypt(decodeBase64(text), key);
    result = new String(decryptText, StandardCharsets.UTF_8);
    return result;
  }

  /**
   * Generates Private Key from BASE64 encoded string.
   *
   * @param key BASE64 encoded string which represents the key.
   * @return The PrivateKey.
   * @throws InvalidKeySpecException class.
   * @throws NoSuchAlgorithmException class.
   */
  private PrivateKey getPrivateKeyFromString(String key) throws InvalidKeySpecException, NoSuchAlgorithmException {
    KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM_RSA);
    EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(decodeBase64(key));
    return keyFactory.generatePrivate(privateKeySpec);
  }

  /**
   * Decode BASE64 encoded string to bytes array.
   *
   * @param text The string.
   * @return Bytes array.
   */
  private byte[] decodeBase64(String text) {
    return Base64.decodeBase64(text);
  }

}
