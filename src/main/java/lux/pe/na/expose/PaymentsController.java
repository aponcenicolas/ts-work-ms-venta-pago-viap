package lux.pe.na.expose;

import lombok.AllArgsConstructor;
import lux.pe.na.business.FirstPaymentService;
import lux.pe.na.business.RecurringPaymentService;
import lux.pe.na.expose.request.PrimerPagoRequest;
import lux.pe.na.expose.request.PagoRecurrenteRequest;
import lux.pe.na.expose.response.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping(value = "/api/solicitudes")
@AllArgsConstructor
public class PaymentsController {

  private final FirstPaymentService firstPaymentService;
  private final RecurringPaymentService recurringPaymentService;

  @GetMapping(value = "/{idsolicitudmultiple}/pagos")
  public Mono<ResponseEntity<Response>> getFirstPayment(@PathVariable(value = "idsolicitudmultiple") Integer multipleRequestId) {
    return firstPaymentService.getAllFirstPayment(multipleRequestId)
        .map(response -> ResponseEntity.status(HttpStatus.OK).body(response))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping(value = "/{idsolicitudmultiple}/pagos")
  public Mono<ResponseEntity<Response>> updateFirstPayment(@PathVariable(value = "idsolicitudmultiple") Integer multipleRequestId,
                                                           @RequestBody List<PrimerPagoRequest> primerPagoRequestList) {
    return firstPaymentService.updateFirstPayment(multipleRequestId, primerPagoRequestList)
        .map(response -> ResponseEntity.status(HttpStatus.OK).body(response))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
  }

  @GetMapping(value = "/{idsolicitudmultiple}/pagos/recurrente")
  public Mono<ResponseEntity<Response>> getAllRecurringPayment(@PathVariable(value = "idsolicitudmultiple") Integer multipleRequestId) {
    return recurringPaymentService.getAllRecurringPayment(multipleRequestId)
        .map(response -> ResponseEntity.status(HttpStatus.OK).body(response))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.BAD_REQUEST).build());
  }

  @GetMapping(value = "/{idsolicitudmultiple}/pagos/recurrente")
  public Mono<ResponseEntity<Response>> updateRecurringPayment(@PathVariable(value = "idsolicitudmultiple") Integer multipleRequestId,
                                                               @RequestBody List<PagoRecurrenteRequest> pagoRecurrenteRequestList,
                                                               @RequestParam boolean useSamePaymentMethod) {
    return recurringPaymentService.updateRecurringPayment(multipleRequestId, pagoRecurrenteRequestList, useSamePaymentMethod)
        .map(response -> ResponseEntity.status(HttpStatus.OK).body(response))
        .defaultIfEmpty(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
  }
}
