package lux.pe.na.expose.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PagoRecurrenteRequest {

  private String usuario;
  private Integer idSolicitud;
  private Integer codigoTipoCobranza;
  private Integer codigoOperadorTarjeta;
  private Integer codigoTipoCuenta;
  private String numeroTarjeta;
  private String fechaVencimiento;
}
