package lux.pe.na.expose.request;

import lombok.*;
import lux.pe.na.model.dto.ItemValor;

import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PrimerPagoRequest {
  private String usuario; // Lol
  private String externalId; // Lol
  private Integer idSolicitud; // Lol
  private ItemValor cobranzaPrimeraCuota;// Lol
  private ItemValor entidadPrimeraCuota;// Lol
  private String codigoSafetyPay;// Lol
  private Timestamp fechaVencimientoSafetyPay;// Lol
  private Timestamp fechaPago;// Lol
  private String numeroOperacionTarjeta;// Lol
}
