package lux.pe.na.expose.response;

import lombok.*;
import lux.pe.na.model.dto.PagoRecurrenteDto;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PagoRecurrenteResponse {

  private Boolean usarMismoMedioPago;
  private List<PagoRecurrenteDto> pagoRecurrenteDtoList;
}
