package lux.pe.na.expose.response;

import lombok.*;
import lux.pe.na.model.dto.ItemValor;

import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PrimerPagoResponse {

  public Boolean esPagoLink;
  private Integer idSolicitud;
  private ItemValor cobranzaPrimeraCuota;
  private ItemValor entidadPrimeraCuota;
  private String codigoSafetyPay;
  private Timestamp fechaVencimientoSafetyPay;
  private Timestamp fechaPago;
  private String numeroOperacionTarjeta;
}
