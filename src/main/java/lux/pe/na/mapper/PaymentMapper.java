package lux.pe.na.mapper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lux.pe.na.expose.request.PagoRecurrenteRequest;
import lux.pe.na.expose.request.PrimerPagoRequest;
import lux.pe.na.model.dto.SolicitudDto;
import lux.pe.na.model.entity.Solicitud;

import javax.persistence.Tuple;
import java.sql.Timestamp;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PaymentMapper {

  public static SolicitudDto tupleToRequestDto(Tuple tuple) {
    return SolicitudDto.builder()
        .idSolicitud(Integer.parseInt(tuple.get("name").toString()))
        .numeroSolicitud(Integer.parseInt(tuple.get("NumeroSolicitud").toString()))
        .numeroSolicitudDispositivo(Integer.parseInt(tuple.get("NumeroSolicitudDispositivo").toString()))
        .idSolicitudMultiple(Integer.parseInt(tuple.get("IdSolicitudMultiple").toString()))
        .codigoMultiple(Integer.parseInt(tuple.get("CodigoMultiple").toString()))
        .codigoProducto(tuple.get("CodigoProducto").toString())
        .codigoVersionProducto(tuple.get("CodigoVersionProducto").toString())
        .codigoPaquete(Integer.parseInt(tuple.get("CodigoPaquete").toString()))
        .condicionCesionDerechos(Boolean.parseBoolean(tuple.get("CondicionCesionDerechos").toString()))
        .indicadorAseguradoCobraBeneficio(Boolean.parseBoolean(tuple.get("IndicadorAseguradoCobraBeneficio").toString()))
        .codigoTipoCobranza(Integer.parseInt(tuple.get("CodigoTipoCobranza").toString()))
        .codigoEntidad(Integer.parseInt(tuple.get("CodigoEntidad").toString()))
        .codigoTipoCuenta(Integer.parseInt(tuple.get("CodigoTipoCuenta").toString()))
        .numeroCuentaTarjeta(tuple.get("NumeroCuentaTarjeta").toString())
        .vencimientoTarjeta(tuple.get("VencimientoTarjeta").toString())
        .flagAdjuntoTarjeta(Boolean.parseBoolean(tuple.get("FlagAdjuntoTarjeta").toString()))
        .codigoTipoCobranzaPrimeraCuota(Integer.parseInt(tuple.get("CodigoTipoCobranzaPrimeraCuota").toString()))
        .codigoEntidadPrimeraCuota(Integer.parseInt(tuple.get("CodigoEntidadPrimeraCuota").toString()))
        .codigoSafetyPay(tuple.get("CodigoSafetyPay").toString())
        .fechaVencimientoSafetyPay(Timestamp.valueOf(tuple.get("FechaVencimientoSafetyPay").toString()))
        .fechaPago(Timestamp.valueOf(tuple.get("FechaPago").toString()))
        .fechaOperacionTarjeta(Timestamp.valueOf(tuple.get("FechaOperacionTarjeta").toString()))
        .numeroOperacionTarjeta(tuple.get("NumeroOperacionTarjeta").toString())
        .codigoRespuestaTarjeta(tuple.get("CodigoRespuestaTarjeta").toString())
        .sumaAseguradaCoberturaPrincipal(Double.parseDouble(tuple.get("SumaAseguradaCoberturaPrincipal").toString()))
        .usarMismoMedioPago(Boolean.parseBoolean(tuple.get("UsarMismoMedioPago").toString()))
        .numeroCuotaFU(Integer.parseInt(tuple.get("NumeroCuotasFU").toString()))
        .montoFondoUniversitario(Double.parseDouble(tuple.get("MontoFondoUniversitario").toString()))
        .flagPagoLink(Boolean.parseBoolean(tuple.get("FlagPagoLink").toString()))
        .build();
  }

  public static Solicitud firstPaymentRequestToRequest(PrimerPagoRequest firstPaymentRequest) {
    return Solicitud.builder()
        .usuarioCreacion(firstPaymentRequest.getUsuario())
        .externalId(firstPaymentRequest.getExternalId())
        .idSolicitud(firstPaymentRequest.getIdSolicitud())
        .codigoTipoCobranza(firstPaymentRequest.getCobranzaPrimeraCuota().getCodigo())
        .codigoEntidadPrimeraCuota(firstPaymentRequest.getEntidadPrimeraCuota().getCodigo())
        .codigoSafetyPay(firstPaymentRequest.getCodigoSafetyPay())
        .fechaVencimientoSafetyPay(firstPaymentRequest.getFechaVencimientoSafetyPay().toString())
        .fechaPago(firstPaymentRequest.getFechaPago())
        .numeroOperacionTarjeta(firstPaymentRequest.getNumeroOperacionTarjeta())
        .build();
  }

  public static Solicitud recurringPaymentToRequest(PagoRecurrenteRequest recurringPaymentRequest){
    return Solicitud.builder()
        .usuarioCreacion(recurringPaymentRequest.getUsuario())
        .idSolicitud(recurringPaymentRequest.getIdSolicitud())
        .codigoTipoCobranza(recurringPaymentRequest.getCodigoTipoCobranza())
        .codigoTipoCuenta(recurringPaymentRequest.getCodigoTipoCuenta())
        .codigoEntidad(recurringPaymentRequest.getCodigoOperadorTarjeta())
        .numeroCuentaTarjeta(recurringPaymentRequest.getNumeroTarjeta())
        .vencimientoTarjeta(recurringPaymentRequest.getFechaVencimiento())
        .build();
  }

}
