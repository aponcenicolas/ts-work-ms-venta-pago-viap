package lux.pe.na.model.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ItemValor {
  private Integer codigo;
  private String descripcion;
}
