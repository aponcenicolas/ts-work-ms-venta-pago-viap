package lux.pe.na.model.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PagoRecurrenteDto {

  private Integer idSolicitud;
  private Integer codigoTipoCobranza;
  private Integer codigoOperadorTarjeta;
  private Integer codigoTipoCuenta;
  private String numeroTarjeta;
  private String fechaVencimiento;
}
