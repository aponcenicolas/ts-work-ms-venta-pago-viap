package lux.pe.na.model.dto;

import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SolicitudDto {

  private Integer idSolicitud;
  private Integer numeroSolicitud;
  private Integer numeroSolicitudDispositivo;
  private Integer idSolicitudMultiple;
  private Integer codigoMultiple;
  private String codigoProducto; //3
  private String codigoVersionProducto; //2
  private Integer codigoPaquete;
  private Boolean condicionCesionDerechos;
  private Boolean indicadorAseguradoCobraBeneficio;
  private Integer codigoTipoCobranza;
  private Integer codigoEntidad;
  private Integer codigoTipoCuenta;
  private String numeroCuentaTarjeta; //25
  private String vencimientoTarjeta; //6
  private Boolean flagAdjuntoTarjeta;
  private Integer codigoTipoCobranzaPrimeraCuota;
  private Integer codigoEntidadPrimeraCuota;
  private String codigoSafetyPay; //15
  private Timestamp fechaVencimientoSafetyPay; //100
  private Timestamp fechaPago;
  private Timestamp fechaOperacionTarjeta;
  private String numeroOperacionTarjeta; //15
  private String codigoRespuestaTarjeta; //1
  private Double sumaAseguradaCoberturaPrincipal;
  private Boolean usarMismoMedioPago;
  private Integer numeroCuotaFU;
  private Double montoFondoUniversitario;
  private Boolean flagPagoLink;

}
