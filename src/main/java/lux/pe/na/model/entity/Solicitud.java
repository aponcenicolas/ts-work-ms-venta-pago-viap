package lux.pe.na.model.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "Solicitud", schema = "Solicitud")
public class Solicitud {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "IdSolicitud")
  private Integer idSolicitud;

  @Column(name = "NumeroSolicitud")
  private Integer numeroSolicitud;

  @Column(name = "NumeroSolicitudDispositivo")
  private Integer numeroSolicitudDispositivo;

  @Column(name = "IdSolicitudMultiple")
  private Integer idSolicitudMultiple;

  @Column(name = "CodigoProducto", length = 3)
  private String codigoProducto;

  @Column(name = "CodigoVersionProducto", length = 2)
  private String codigoVersionProducto;

  @Column(name = "CodigoPaquete")
  private Integer codigoPaquete;

  @Column(name = "CodigoFrecuenciaPago")
  private Integer codigoFrecuenciaPago;

  @Column(name = "CodigoFondoInversionCI")
  private Integer codigoFondoInversionCI;

  @Column(name = "CodigoFondoInversionCE")
  private Integer codigoFondoInversionCE;

  @Column(name = "CodigoEsquemaSumaAsegurada")
  private Integer codigoEsquemaSumaAsegurada;

  @Column(name = "PrimaTarget")
  private Double primaTarget;

  @Column(name = "DerechoEmision")
  private Double derechoEmision;

  @Column(name = "PrimaExcedente")
  private Double primaExcedente;

  @Column(name = "Igv")
  private Double igv;

  @Column(name = "PrimaTotal")
  private Double primaTotal;

  @Column(name = "NumeroCuotasFU")
  private Integer numeroCuotasFU;

  @Column(name = "MontoFondoUniversitario")
  private Double montoFondoUniversitario;

  @Column(name = "CondicionCesionDerechos")
  private Boolean condicionCesionDerechos;

  @Column(name = "IndicadorAseguradoCobraBeneficio")
  private Boolean indicadorAseguradoCobraBeneficio;

  @Column(name = "CodigoTipoCobranza")
  private Integer codigoTipoCobranza;

  @Column(name = "CodigoEntidad")
  private Integer codigoEntidad;

  @Column(name = "CodigoTipoCuenta")
  private Integer codigoTipoCuenta;

  @Column(name = "NumeroCuentaTarjeta", length = 25)
  private String numeroCuentaTarjeta;

  @Column(name = "VencimientoTarjeta", length = 6)
  private String vencimientoTarjeta;

  @Column(name = "FlagAdjuntoTarjeta")
  private Boolean flagAdjuntoTarjeta;

  @Column(name = "IndicadorSolicitoMovimientoBancario")
  private Integer indicadorSolicitoMovimientoBancario;

  @Column(name = "FlagAdjuntoMovimientoBancario")
  private Boolean flagAdjuntoMovimientoBancario;

  @Column(name = "FlagMovimientoBancarioValido")
  private Boolean flagMovimientoBancarioValido;

  @Column(name = "FechaCreacion")
  private Timestamp fechaCreacion;

  @Column(name = "UsuarioCreacion", length = 20)
  private String usuarioCreacion;

  @Column(name = "FechaModificacion")
  private Timestamp fechaModificacion;

  @Column(name = "UsuarioModificacion", length = 20)
  private String usuarioModificacion;

  @Column(name = "CodigoTipoCobranzaPrimeraCuota")
  private Integer codigoTipoCobranzaPrimeraCuota;

  @Column(name = "CodigoEntidadPrimeraCuota")
  private Integer codigoEntidadPrimeraCuota;

  @Column(name = "CodigoSafetyPay", length = 15)
  private String codigoSafetyPay;

  @Column(name = "FechaVencimientoSafetyPay", length = 100)
  private String fechaVencimientoSafetyPay;

  @Column(name = "FechaPago")
  private Timestamp fechaPago;

  @Column(name = "FechaOperacionTarjeta")
  private Timestamp fechaOperacionTarjeta;

  @Column(name = "NumeroOperacionTarjeta", length = 15)
  private String numeroOperacionTarjeta;

  @Column(name = "CodigoRespuestaTarjeta", length = 1)
  private String codigoRespuestaTarjeta;

  @Column(name = "SumaAseguradaCoberturaPrincipal")
  private Double sumaAseguradaCoberturaPrincipal;

  @Column(name = "FlagPagoLink")
  private Integer flagPagoLink;

  @Column(name = "ExternalId", length = 50)
  private String externalId;
}
