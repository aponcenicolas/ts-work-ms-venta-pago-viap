package lux.pe.na.model.entity;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "SOLICITUD_MULTIPLE", schema = "Solicitud")
public class SolicitudMultiple {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "IdSolicitudMultiple")
  private Integer idSolicitudMultiple;

  @Column(name = "CodigoMultiple")
  private Integer codigoMultiple;

  @Column(name = "CodigoCotizacion")
  private String codigoCotizacion; // 10

  @Column(name = "CodigoAgencia")
  private Integer codigoAgencia;

  @Column(name = "CodigoOficina")
  private Integer codigoOficina;

  @Column(name = "CodigoIntermediario")
  private Integer codigoIntermediario;

  @Column(name = "CodigoModalidad")
  private Integer codigoModalidad;

  @Column(name = "CodigoCentroMedico")
  private Integer codigoCentroMedico;

  @Column(name = "FechaCitaExamenMedico")
  private Timestamp fechaCitaExamenMedico;

  @Column(name = "DireccionExamenMedico")
  private String direccionExamenMedico; // 250

  @Column(name = "FechaFirma")
  private Timestamp fechaFirma;

  @Column(name = "IdProspecto")
  private Integer idProspecto;

  @Column(name = "NecesidadEstimadaVidaDolares")
  private Double necesidadEstimadaVidaDolares;

  @Column(name = "CodigoTipoCobranzaPrimeraCuota")
  private Integer codigoTipoCobranzaPrimeraCuota;

  @Column(name = "CodigoEntidadPrimeraCuota")
  private Integer codigoEntidadPrimeraCuota;

  @Column(name = "CodigoSafetyPay")
  private String codigoSafetyPay; //15

  @Column(name = "FechaVencimientoSafetyPay")
  private String fechaVencimientoSafetyPay; //100

  @Column(name = "FechaPago")
  private Timestamp fechaPago;

  @Column(name = "FechaOperacionTarjeta")
  private Timestamp fechaOperacionTarjeta;

  @Column(name = "NumeroOperacionTarjeta")
  private String numeroOperacionTarjeta; //15

  @Column(name = "CodigoRespuestaTarjeta")
  private String codigoRespuestaTarjeta; //1

  @Column(name = "EvicertiaUniqueId")
  private String evicertiaUniqueId; //200

  @Column(name = "CodigoEstado")
  private Integer codigoEstado;

  @Column(name = "FechaEstadoActual")
  private Timestamp fechaEstadoActual;

  @Column(name = "CodigoEstadoTransferencia")
  private Integer codigoEstadoTransferencia;

  @Column(name = "FlagEnviarOnBase")
  private Boolean flagEnviarOnBase;

  @Column(name = "FlagEnviarSolicitudDigital")
  private Boolean flagEnviarSolicitudDigital;

  @Column(name = "FechaCreacionDispositivo")
  private Timestamp fechaCreacionDispositivo;

  @Column(name = "FechaModificacionDispositivo")
  private Timestamp fechaModificacionDispositivo;

  @Column(name = "FlagTransaccionConfirmada")
  private Boolean flagTransaccionConfirmada;

  @Column(name = "FechaCreacion")
  private Timestamp fechaCreacion;

  @Column(name = "UsuarioCreacion")
  private String usuarioCreacion; //20

  @Column(name = "FechaModificacion")
  private Timestamp fechaModificacion;

  @Column(name = "UsuarioModificacion")
  private String usuarioModificacion; //20

  @Column(name = "FlagCotizacionAutorizada")
  private Boolean flagCotizacionAutorizada;

  @Column(name = "MonedaNecesidadEstimadaVida")
  private Integer monedaNecesidadEstimadaVida;

  @Column(name = "NecesidadEstimadaVidaSoles")
  private Double necesidadEstimadaVidaSoles;

  @Column(name = "TipoFirma")
  private Integer tipoFirma;

  @Column(name = "FlagSuscripcionInteligente")
  private Integer flagSuscripcionInteligente;

  @Column(name = "FlagMismoMedioPagoRecurrente")
  private Boolean flagMismoMedioPagoRecurrente;

  @Column(name = "IndicadorLpdp")
  private Integer indicadorLpdp;

  @Column(name = "FlagSolicitudFirma")
  private Boolean flagSolicitudFirma;

}
