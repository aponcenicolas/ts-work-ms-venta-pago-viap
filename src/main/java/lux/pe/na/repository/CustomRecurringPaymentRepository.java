package lux.pe.na.repository;

import lux.pe.na.model.dto.SolicitudDto;

import java.util.List;

public interface CustomRecurringPaymentRepository {

  List<SolicitudDto> findAllRequest(Integer multipleRequestId);
}
