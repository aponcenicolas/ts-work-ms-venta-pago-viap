package lux.pe.na.repository;

import lux.pe.na.model.entity.SolicitudMultiple;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MultipleRequestRepository extends JpaRepository<SolicitudMultiple, Integer> {
}
