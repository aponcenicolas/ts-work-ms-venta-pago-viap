package lux.pe.na.repository;

import lux.pe.na.model.entity.Solicitud;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestRepository extends JpaRepository<Solicitud, Integer>, CustomRecurringPaymentRepository {

  List<Solicitud> findByIdSolicitudMultiple(Integer multipleRequestId);
}
