package lux.pe.na.repository.impl;

import lux.pe.na.mapper.PaymentMapper;
import lux.pe.na.model.dto.SolicitudDto;
import lux.pe.na.repository.CustomRecurringPaymentRepository;
import lux.pe.na.util.RequestQuery;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

import static lux.pe.na.util.RequestQuery.*;

public class CustomRecurringPaymentRepositoryImpl implements CustomRecurringPaymentRepository {

  @PersistenceContext
  private EntityManager entityManager;

  @Override
  public List<SolicitudDto> findAllRequest(Integer multipleRequestId) {
    Query query = entityManager.createNativeQuery(GET_ALL_REQUEST, Tuple.class);
    List<Tuple> requestList = query.getResultList();
    return requestList.stream()
        .map(PaymentMapper::tupleToRequestDto)
        .collect(Collectors.toList());
  }
}
