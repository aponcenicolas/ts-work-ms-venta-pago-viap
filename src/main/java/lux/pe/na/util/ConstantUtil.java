package lux.pe.na.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConstantUtil {

  public static final String MESSAGE_UPDATED_SUCCESS = "Actualizado correctamente";
  public static final String MESSAGE_UPDATED_ERROR = "No existen datos para actualizar";
  public static final String VALUE_ONE = "1";
}
