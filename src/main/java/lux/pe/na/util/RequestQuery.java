package lux.pe.na.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RequestQuery {

  public static final String GET_ALL_REQUEST = "SELECT SO.IdSolictud, \n" +
      "SO.NumeroSolicitud, \n" +
      "SO.NumeroSolicitudDispositivo, \n" +
      "SO.IdSolicitudMultiple, \n" +
      "SM.CodigoMultiple, \n" +
      "SO.CodigoProducto, \n" +
      "SO.CodigoVersionProducto, \n" +
      "ISNULL(SO.CodigoPaquete, 0) AS CodigoPaquete, \n" +
      "SO.CondicionCesionDerechos, \n" +
      "SO.IndicadorAseguradoCobraBeneficio, \n" +
      "SO.CodigoTipoCobranza, \n" +
      "SO.CodigoEntidad, \n" +
      "SO.CodigoTipoCuenta, \n" +
      "SO.NumeroCuentaTarjeta, \n" +
      "SO.VencimientoTarjeta, \n" +
      "SO.FlagAdjuntoTarjeta, \n" +
      "SO.CodigoTipoCobranzaPrimeraCuota, \n" +
      "SO.CodigoEntidadPrimeraCuota, \n" +
      "SO.CodigoSafetyPay, \n" +
      "SO.FechaVencimientoSafetyPay, \n" +
      "SO.FechaPago, \n" +
      "SO.FechaOperacionTarjeta, \n" +
      "SO.NumeroOperacionTarjeta, \n" +
      "SO.CodigoRespuestaTarjeta, \n" +
      "SO.SumaAseguradaCoberturaPrincipal, \n" +
      "SM.FlagMismoMedioPagoRecurrente AS UsarMismoMedioPago, \n" +
      "SO.NumeroCuotasFU, \n" +
      "SO.MontoFondoUniversitario, \n" +
      "SO.FlagPagoLink \n" +
      "FROM Solicitud.SOLICITUD SO \n" +
      "INNER JOIN Solicitud.SOLICITUD_MULTIPLE SM \n" +
      "ON SO.IdSolicitudMultiple = SM.IdSolicitudMultiple \n" +
      "WHERE SM.IdSolicitudMultiple >= :multipleRequestId ";
}
